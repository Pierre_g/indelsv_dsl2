

process CONCATVCF{
    publishDir path:"${params.output}/vcf/",mode: 'copy', overwrite: true, failOnError:true
    conda "${params.conda_envs}/makexcel"
    cpus 6
    memory '24G'
    input:
    tuple val(sample), path(allVCF)

    output:

    tuple val(sample),path("Nk_${sample}_SV.vcf")

    script:
    """
    python3 $baseDir/concatSvVCF.py -i ./ -o ./Nk_${sample}.tmpvcf
    bcftools sort Nk_${sample}.tmpvcf > Nk_${sample}_SV.vcf
    """

}

process MAKEXLSX{
    publishDir path:"${params.output}/xlsx/",mode: 'copy', overwrite: true, failOnError:true
    conda "${params.conda_envs}/makexcel"
    cpus 6
    memory '24G'
    input:
    tuple val(sample), path(concatVCF)

    output:

    tuple val(sample),path("${sample}_TEST_.xlsx")

    script:
    """
    python3 /home/kave/depotGit/vcf2xlsx/makeExcel.py -i ${concatVCF} -o ./ \
    -n ${sample} -r ${params.project_name} -nk TESTCNV
    """

}

workflow CONCATRESULTS {

    take:
        data
    main:
        CONCATVCF(data)
        CONCATVCF.out.multiMap{it ->
            toXLSX : it
            toEmit : it}
            .set{ VCFTOCONVERT }

        MAKEXLSX(VCFTOCONVERT.toXLSX)

    emit:
        VCFTOCONVERT.toEmit
}

