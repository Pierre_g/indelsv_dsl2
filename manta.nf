
process MANTA {
    conda "${params.conda_envs}/manta"
    publishDir path:"${params.output}/brut/manta",mode: 'copy', overwrite: true, failOnError:true
    cpus 4
    memory '10 GB'
    tag "${ID_ECH}"

    input:
    tuple val(ID_ECH),path(BAM_FILE)


    output:

    tuple val(ID_ECH),path("${ID_ECH}_manta.vcf")

    script:
    // --callRegions ${map_parameters.design}.bed
    """
    ${params.mantaconfigPath} --bam ${BAM_FILE[0]} --exome --referenceFasta\
    ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa \
     --callMemMb ${task.memory.toMega()} \
    --scanSizeMb 12 

    python MantaWorkflow/runWorkflow.py -j ${task.cpus} -g ${task.memory.toMega()}

    #Concatenate all VCF Candidate and Validate SV
    mv MantaWorkflow/results/variants/* . 
    touch tmpCandidate.vcf
    gunzip -c candidateSmallIndels.vcf.gz | grep -v "#" > tmpCandidate.vcf
    gunzip -c candidateSV.vcf.gz | grep -v "#" >> tmpCandidate.vcf
    touch ${ID_ECH}_manta_tmp.vcf
    gunzip -c diploidSV.vcf.gz > ${ID_ECH}_manta_tmp.vcf
    awk 'BEGIN{ FS="\t";OFS="\t"}{print \$0,"GT:FT:GQ:PL:PR:SR",".:.:.:.:.:."}' tmpCandidate.vcf >> ${ID_ECH}_manta_tmp.vcf
    cat ${ID_ECH}_manta_tmp.vcf | grep "##" > ${ID_ECH}_manta.vcf
    gunzip -c candidateSmallIndels.vcf.gz | grep "#" | grep "COUNT" >> ${ID_ECH}_manta.vcf
    cat ${ID_ECH}_manta_tmp.vcf | grep "#C" >> ${ID_ECH}_manta.vcf
    cat ${ID_ECH}_manta_tmp.vcf | grep -v "#" >> ${ID_ECH}_manta.vcf

    """
}