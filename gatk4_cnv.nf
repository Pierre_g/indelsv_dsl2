process READCOUNT {
	
	tag "${sample}"
    publishDir "${params.output}/brut//gatk/readcount/", mode : 'copyNoFollow', overwrite:true

	/*
	Totalise les nombres entiers bruts de lectures chevauchant un intervalle.
    L'utilisation d'un fichier interval_list est requis et implique forcémnent 
    l'utilisation de l'argument OVERLAPPING_ONLY (gatk 4.2.0.0)

	WARN: 
    L'outil emploie des filtres. Tous les reads marqués comme `duplicate` 
    et avec un MAPQ < 40 sont exclus

    OUTPUT: 
    Fichier HDF5 contenant les comptages brut
	*/
	input:
	    tuple val(sample), path(bam) 

    output:	
		tuple val(sample), path("${sample}.hdf5") 

	script:
	"""
        ${params.path_gatk} CollectReadCounts -R ${params.path_reference} \
        -imr OVERLAPPING_ONLY -I ${sample}.bam -O ${sample}.hdf5 --minimum-mapping-quality 40 \
        --intervals ${params.interval_list}
	"""
}

process DETERPLOIDY{
	conda "${params.conda_envs}/gcnv"
	/*
    Determine une ligne de base de la ploidie de chaque contig
    en utilisant le compte de read	par contig. 
    Il permet de faire du caryotypage à partir d'un modèle de données
    afin de palier aux biais techniques et de variabilité selon la technique

    Si option --model,mode utilisé: CASE.

    


	Dans ce mode, les paramètres du modèle de ploïdie sont chargés à partir du répertoire fourni et
	seules les quantités spécifiques à l'échantillon sont déduites. Les intervalles modélisés sont 
	ensuite spécifiés par un fichier contenu dans le répertoire du modèle, tous les arguments liés à 
	l'intervalle sont ignorés dans ce mode et tous les intervalles du modèle doivent être présents dans
	tous les fichiers de comptage d'entrée.


	Output: 
	 La sortie de l'outil en mode CASE est uniquement le
	sous-répertoire "-calls" avec:
        - contig_ploidy.tsv : 
            Fichier tsv contenant la ploidy par chromosome avec la qualité de génotypage (GQ)
            (Les régions avec un faible GQ auront des qualités d'appels de CNV plus faible)
        - gloabl_read_depth.tsv
            TSV avec deux colonnes: 
            - Profondeur globale sur tous les intervals
            - Ploidy moyenne
        - mu_psi_s_log__.tsv :
            - Capture la moyenne à postériori(https://fr.wikipedia.org/wiki/Probabilit%C3%A9_a_posteriori)
            des tous les paramètres du modèle généré
        - std_psi_s_log__.tsv:
            - Estimateur de l'écart type de tous les paramètres du modèle
        - sample_name.txt


	*/
	publishDir "${params.output}/brut/gatk/ploidy/",mode:'copy',overwrite: true,pattern:"*ploidy-calls*"
	tag "${sample}"
	input:
		tuple val(sample), path (hdf5) 
		
	output:
		tuple val(sample), path("${params.project_name}_${sample}_ploidy*"), emit: ploidy
		tuple val(sample), path(hdf5), emit: count_hdf5

	script:
    if (params.default)
    """
	${params.path_gatk} DetermineGermlineContigPloidy --model ${params.pathModelPloidy} \
	-I ${hdf5} --output . --output-prefix ${params.project_name}_${sample}_ploidy --verbosity DEBUG \
	"""
    else
	"""
	${params.path_gatk} DetermineGermlineContigPloidy --model ${params.ModelPloidy} \
	-I ${hdf5} --output . --output-prefix ${params.project_name}_${sample}_ploidy --verbosity DEBUG \
	--mean-bias-standard-deviation ${params.DCGmeanBiaSDev} --mapping-error-rate ${params.mapErrorRate} \
	--global-psi-scale ${params.DCGGlobPsiScale} --sample-psi-scale ${params.SamplePsiScale}
	"""

}


process CALLCNV {
	/*
    Appels des CNVs à partir de la profondeur entre le modèle et les counts de l'échantillon


	NB: Cet outil ne vérifie pas automatiquement la compatibilité de la paramétrisation 
		fournie avec les fichiers de comptage fournis. La compatibilité du modèle par rapport aux données
        peut être évaluée a posteriori en inspectant l'ampleur de la variance inexpliquée 
		spécifique à l'échantillon et en affirmant qu'elles se situent dans la même plage
		que celles obtenues à partir de la cohorte utilisée pour générer la paramétrisation.

    Output:
		Dossier tracking
			- elbo_history.tsv: ELBO pour "evidence lower bound" correspondant à une quantité utilisée dans les modèles bayésien
			pour estimer la qualité de l'ajustement d'un modèle (i.e profondeur de séquençage du jeu de référence). 
			Chaque ligne correspondrait à un score de qualité pour chaque itération d'ajustement du modèle. 
			Plus une valeur est élevé plus l'ajustement est bon (relativement aux autres)
		Dossier calls
			- calling_config.json
			- denoising_config.json
			- gcnvkernel_version.json
			- interval_list: TSV avec le taux en GC et la mappabilité pour chaque régions cible
			- SAMPLE_0/ (A confirmer avec GATK)
				- sample_name.txt
				- baseline_copy_number_t.tsv : Nombre de copie de base pour chaque régions cible (interval_list.tsv)
				- log_c_emission_tc.tsv & log_q_c_tc.tsv: Tableaux des log de probabilités d'état de copie pour chaque interval cible.
				Utilisés pour le calling final afin de calculer la probabilité à postériori de chaque état de copie. 
				- mu_denoised_copy_ratio_t.tsv & std_denoised_copy_ratio_t: Ratio débruité pour chaque interval. Le débruitage implique d'ajuster
				une modèle de Markov caché aux profondeurs de lectures et à estimer les états du nombre de copies et leur paramètres
				(moyenne(mu) et variance (std))
				- mu_psi_s_log__.tsv : ? 
				- mu_read_depth_s_log__.tsv : ? 
				- mu_z_sg.tsv & std_z_sg.tsv (???) : Z-score du ratio de nombre de copie pour chaque 
				interval par rapport aux échantillons appariés selon le sexe
				- mu_z_su.tsv & std_z_su.tsv (???) : Z-score du ratio de nombre de copie pour chaque 
				interval par rapport aux échantillons de la cohorte
	*/
	conda "${params.conda_envs}/gcnv"
	publishDir "${params.output}/brut//gatk/callCNV/", mode : 'copyNoFollow', overwrite: true	
	tag "${sample}"
	input: 
		tuple val(sample), path(read), path(ploidyCalls) 

	output:
		tuple val(sample), path ("${params.project_name}_${sample}_callGermCNV-calls"),
        path("${params.project_name}_${sample}_callGermCNV-tracking") 

	script:
    if (params.default)
    """
	${params.path_gatk} GermlineCNVCaller --run-mode CASE -I ${read} \
    --contig-ploidy-calls ${ploidyCalls} \
	--model ${params.pathCallingModel} --output . \
    --output-prefix ${params.project_name}_${sample}_callGermCNV\
	--verbosity DEBUG \
	"""

    else
	"""
	${params.path_gatk} GermlineCNVCaller --run-mode CASE -I ${read} --contig-ploidy-calls ${ploidyCalls} \
	--model ${params.pathCallingModel} --output . \
    --output-prefix ${params.project_name}_${sample}_callGermCNV\
	--verbosity DEBUG \
	--class-coherence-length ${params.GCclassCoheLen} --cnv-coherence-length ${params.GCclassCNVLen} \
	--log-mean-bias-standard-deviation ${params.GCLogMeanSD} --sample-psi-scale ${params.SamplePsiScale} \
	  --interval-psi-scale ${params.intervPsiScale}
	"""
}

process POSTPROC {
	/* 
	Post-traite les sorties de l'appel de CNV de GATK, afin de consolider 
	les résultats dispersés de GermlineCNVCaller, effectue une segmentation
	et apelle les état de num de copie. 

	Output:
		- intervals (VCF):
			- Fournit le nombre de copie le plus probable pour chaque interval génomique
			inclus aussi la qualité des appels, genotype (GT) le vecteur de probabilité postérieur (PL ?).

		- segments (VCF):
			Fusion des intervals génomique avec le même nombre de copie afin de segmenter le fichiers en
			fonction des états de copies de chaque segment (avec GT recalculé)

	
	*/

	conda "${params.conda_envs}/gcnv"
	publishDir "${params.output}/brut/gatk/postprocess/", mode : 'copyNoFollow',	overwrite: true
	
	input: 
		tuple val(sample),path("${params.project_name}_${sample}_callGermCNV-calls"),path("${params.project_name}_${sample}_callGermCNV-tracking"),path("${params.project_name}_${sample}_ploidy-calls")
	    
	output:
		tuple val(sample), path("${sample}_${params.project_name}*"), emit: outputGatk
		tuple val(sample), path("${sample}_${params.project_name}_gatk_segments.vcf"), emit: vcfGatk
	script:
	"""
	${params.path_gatk} PostprocessGermlineCNVCalls \
    --contig-ploidy-calls ${params.project_name}_${sample}_ploidy-calls \
    --model-shard-path ${params.pathCallingModel} \
    --calls-shard-path ./${params.project_name}_${sample}_callGermCNV-calls \
    --allosomal-contig chrX \
    --allosomal-contig chrY \
    --sample-index 0 \
    --autosomal-ref-copy-number 2 \
    --output-genotyped-intervals ${sample}_${params.project_name}_intervals.vcf \
    --output-genotyped-segments ${sample}_${params.project_name}_segments_brut.vcf \
    --output-denoised-copy-ratios ${sample}_${params.project_name}_denoised_copy_ratios.tsv

	grep -v "#" ${sample}_${params.project_name}_segments_brut.vcf | awk 'BEGIN{FS="\t";OFS="\t"} { if (\$5 ~ /DUP/) \
	{print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8";SVTYPE=DUP",\$9,\$10} else if (\$5 ~ /DEL/) \
	{print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8";SVTYPE=DEL",\$9,\$10}else \
	{print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8";SVTYPE=.",\$9,\$10}}' > tmpVCF.vcf

	grep "#" ${sample}_${params.project_name}_segments_brut.vcf > ${sample}_${params.project_name}_gatk_segments.vcf
	cat tmpVCF.vcf >> ${sample}_${params.project_name}_gatk_segments.vcf




    """

} 

// grep -v "#" ${sample}_${params.project_name}_segments_brut.vcf | awk 'BEGIN{FS="\t";OFS="\t"} { if (\$5 ~ /DUP/) \
// {print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8"\;SVTYPE=DUP",\$9,\$10} else if (\$5 ~ /DEL/) \
// {print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8";SVTYPE=DEL",\$9,\$10}else \
// {print \$1,\$2,\$3,\$4,\$5,\$6,\$7,\$8";SVTYPE=.",\$9,\$10}}' > tmpVCF.vcf

// grep "#" ${sample}_${params.project_name}_segments_brut.vcf > ${sample}_${params.project_name}_segments.vcf
// cat tmpVCF.vcf >> ${sample}_${params.project_name}_segments.vcf

workflow GATK{

    take:
        data
    main: 

		
        READCOUNT(data)
        DETERPLOIDY(READCOUNT.out)

        DETERPLOIDY.out.ploidy
            .multiMap{ it ->
                ploidyToCall : it
                ploidyToPostProc : it}
            .set { ploidy_results }


        CALLCNV(DETERPLOIDY.out.count_hdf5.join(ploidy_results.ploidyToCall))


        CALLCNV.out.join(ploidy_results.ploidyToPostProc).set{ ch_postProc }


        POSTPROC(ch_postProc)

	emit:
		POSTPROC.out.vcfGatk
		// val("gatk")
}
