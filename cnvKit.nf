

process TARGET {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true
    cpus 6
    memory '200G'

    input:
        val(bedDesign)
    output:
        path("*target.bed")

    script:
    """
    name=\$(basename ${bedDesign} .bed)
    cnvkit.py target ${bedDesign} --split -o \${name}_target.bed
    """
}



process ANTITARGET {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true
    cpus 6
    memory '200G'

    input:
        path(targetedRegions)
    output:
        tuple path(targetedRegions),path("*_antitarget.bed")

    script:
    """
    name=\$(basename ${targetedRegions} _target.bed)
    cnvkit.py antitarget ${targetedRegions} -o \${name}_antitarget.bed
    """
}




process REFERENCE {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'

    input:
        tuple path(targetRegions),path(antitargetRegions)
    output:
        path("FlatReference.cnn")

    script:
    """
    cnvkit.py reference -o FlatReference.cnn -f \
    ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa \
    -t ${targetRegions} -a ${antitargetRegions}
    """


}

process COVERAGE {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    tag "${sample}"

    input:
        tuple val(sample),path(BAM_FILES)
        tuple path(targetRegions),path(antitargetRegions)
    output:

        tuple val(sample),path("${sample}.targetcoverage.cnn"),path("${sample}.antitargetcoverage.cnn")


    script:
    """
    cnvkit.py coverage ${sample}.bam ${targetRegions} -o ${sample}.targetcoverage.cnn -p 40
    cnvkit.py coverage ${sample}.bam ${antitargetRegions} -o ${sample}.antitargetcoverage.cnn -p 40
    """
}

process FIX {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    tag "${sample}"

    input:
         tuple val(sample),path(targetCoverage),path(antitargetCoverage)
         path(reference)
    output:
         tuple val(sample),path("${sample}.cnr")

    script:
    """
    cnvkit.py fix ${targetCoverage} ${antitargetCoverage} ${reference} -o ${sample}.cnr
    """
}

process SEGMENTATION {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    tag "${sample}"

    input:
         tuple val(sample),path(ratioFile)
    output:
         tuple val(sample),path("${sample}_segment.cns")

    script:
    """
    cnvkit.py segment ${ratioFile} -o ${sample}_segment.cns -m cbs -p ${task.cpus}
    """

}

process CALL {
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    tag "${sample}"
    input:
        tuple val(sample),path(segmentFile) //,path(snpvcf) // A confirmer pour VCF
    output:
        tuple val(sample),path("${sample}.cns")

    script:
    """
    cnvkit.py call  ${segmentFile} -m none -o ${sample}.cns
    """
}

process EXPORTVCF{
    conda "${params.conda_envs}/cnvkit"
    publishDir path:"${params.output}/brut/cnvkit",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    tag "${sample}"
    input:
        tuple val(sample),path(callFile) //,path(snpvcf) // A confirmer pour VCF
    output:
        tuple val(sample),path("${sample}_cnvkit.vcf")

    script:
    """
    cnvkit.py export vcf ${callFile} -o ${sample}_cnvkit.vcf

    """

}
workflow CNVKIT{
    take:
        BAM_FILES
        BED_DESIGN
    main:
        BED_DESIGN
        TARGET(BED_DESIGN)
        ANTITARGET(TARGET.out)

        ANTITARGET.out.multiMap{ bedFiles ->
            toReference : bedFiles
            toCoverage : bedFiles}.set{ BEDFILES }

        
        REFERENCE(BEDFILES.toReference)

        BAM_FILES.combine(BEDFILES.toCoverage).set{ BAMANDREF }

        COVERAGE(BAM_FILES,BEDFILES.toCoverage)

        FIX(COVERAGE.out,REFERENCE.out)

        SEGMENTATION(FIX.out)

        CALL(SEGMENTATION.out)

        EXPORTVCF(CALL.out)

    emit:
        EXPORTVCF.out

}