process ANNOTSV{

    publishDir path:"${params.output}/annotSV",
    mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '24G'
    tag "${sample}"
    input:
        tuple val(sample),path(concatVCF)
    output:
        tuple val(sample),path("${sample}_annotSV.tsv"),path("${sample}_annotSV.unannotated.tsv")
    //AnnotSV doesn't annot small indels,dup,insertion,deletion

    script:
    """
    ~/opt/annotSV/3.3.1/bin/AnnotSV -SVinputFile ${concatVCF} -benignAF 0.01 -annotationMode full\
    -genomeBuild ${params.genome} -includeCI 1 -outputDir ./ -outputFile ${sample}_annotSV \
    -overlap 100 -promoterSize 500 -REreport 1 -REselect1 0 -REselect2 0 -SVminSize 1 -tx RefSeq 

    """

}

workflow ANNOT{
    take:
        data
    main:
        ANNOTSV(data)
    emit:
        ANNOTSV.out
}