import vcfpy
import glob
import re
import copy
import argparse
import pysam
import os
from joblib import delayed,Parallel
import statistics
import logging
from datetime import date
from tqdm import tqdm
from collections import OrderedDict, Counter



def is_vcf_sorted(vcf_path):
    """
    Check if vcf is sorted or not. 
    (Because vcfpy can make mistakes if vcf is not sorted)

    Args:
        vcf_path (str): VCF path

    Returns:
        Boolean: If VCF is sorted or not.
        str: If VCF not sorted, return problematic coord
    """
    vcf = pysam.VariantFile(vcf_path)
    previous_record = None
    excludedChrom = ["chrX","chrY","chrM"]
    for record in vcf:
        if (record.chrom not in excludedChrom) and ("_" not in record.chrom):
            if previous_record is not None and int(record.chrom[3:]) < int(previous_record.chrom[3:]):
                return False,(previous_record.chrom,previous_record.pos,record.chrom,record.pos,"chr")
            if previous_record is not None and record.chrom == previous_record.chrom and record.pos < previous_record.pos:
                return False,(previous_record.chrom,previous_record.pos,record.chrom,record.pos,)
            previous_record = record
    return True


def vcf2Dict(vcfpyReader,sample):
    """
    Convert vcfpy object in python dict. 
    With in key CHROM:POS-END-REF-ALT


    Args:
        vcfpyReader (vcfpy.Reader): Reader object from vcfpy module
        sample (str): sample name

    Returns:
        dict: Dictionnary thath contains all variants of VCF files
    """

    dictVariant = {}

    for record in vcfpyReader:


        #If variant is breakend END point correspond to POS
        if record.INFO.get('SVTYPE') == "BND":

            codeVariant = f"{record.CHROM}:{record.POS}-{record.POS}-{record.REF}-{record.ALT}"
        else:
            codeVariant = f"{record.CHROM}:{record.POS}-{record.INFO.get('END')}-{record.REF}-{record.ALT}"

        if codeVariant not in dictVariant.keys():

            dictVariant[codeVariant]= {"CHR":"","POS":"","REF":"",
                                       "ALT":"","FORMAT":{},"INFOS":{}}

            dictVariant[codeVariant]["CHR"]    = record.CHROM
            dictVariant[codeVariant]["POS"]    = record.POS
            dictVariant[codeVariant]["REF"]    = record.REF
            dictVariant[codeVariant]["ALT"]    = record.ALT
            dictVariant[codeVariant]["QUAL"]   = record.QUAL
            dictVariant[codeVariant]["FILTER"] = record.FILTER

            for format_name in record.FORMAT:
                dictVariant[codeVariant]["FORMAT"][format_name] = record.call_for_sample[sample].data[format_name]

            for info_name,info_value in record.INFO.items():
                    
                dictVariant[codeVariant]["INFOS"][f"{info_name}"] = info_value

    return dictVariant

def collectSV(vcfPath,listCallerPossiblyUsed):
    """
    Identify callers used thanks to regex with 
    caller name in vcf name.Concatenate all variant
    in same dict with caller in key.

    Raises:
        Exception: 
                If caller name is not in vcf name 
                or not in listCallerPossiblyUsed
        Exception: 
                If several samples are detected in
                vcf file
        Exception: 
                If vcf file is not sorted

    Returns:
        dict: dictSV that contains alls variants 
        for same samples, callerName and sample
        name
    """   

    callerIdentified = False
    dictSV = {}
    callerName=""
    for caller in listCallerPossiblyUsed:

        if bool(re.search(caller,os.path.basename(vcfPath),
                          flags=re.IGNORECASE)):
                
                callerIdentified = True
                callerName = copy.deepcopy(caller)
                
    if callerIdentified == False:
            raise Exception(f"""
                    Unable to detect your caller. 

                    Please specify it in filename
                    
                    {os.path.basename(vcfPath)}""")

    if is_vcf_sorted(vcf_path=vcfPath):
        reader = vcfpy.Reader.from_path(vcfPath)

        if len(reader.header.samples.names) == 1:
            sample = reader.header.samples.names[0]
        else:
            raise Exception("""This tools concatenate CNV VCF File from several tools for unique sample.
                                Please provide VCF with one sample""")
        
        if callerName not in dictSV.keys():
            dictSV[callerName] = {}
    
        dictSV[callerName]["Header"] = reader.header

        dictSV[callerName]["Variants"] = vcf2Dict(reader,sample)
        dictSV["sample"] = sample
        dictSV["caller"] = callerName
    else:
         raise Exception(f"Your VCF is not sorted {vcfPath}")
        
    return dictSV



def notNone(x):

    if x==None:
        return False
    else:
        return True

def concatSV(dictBrut,idSVList,listCaller,NkIdsList):
    """
    Concatenate all variants and variant metadata in
    same dict. All FORMAT and INFOS values are convert
    to string, and their number are converted to 1. 
    Moreover if variants is called by several callers
    all INFOS and FORMAT value category are modify to
    add Nk on both sides. After if category contains "Nk"


    Args:
        dictBrut (dict): Python dict with all variant in key
        idSVList (list): List of unique variant ids detected
                             in all vcf files
        listCaller(list): list of all caller used 
        NkIdsList (list): list of all category that contains Nk 
                            (i.e conlict in category between all vcf files)

    Raises:
        Exception: If idSVlist size is different to dictSV size

    Returns:
        dict: dictSV
    """
    dictSVtmp = {}
    dictSV = {}
    
    for idSV in idSVList:

        callerDetected = []

        #Initialize all dicts with idSV
        if idSV not in dictSVtmp.keys():
            dictSVtmp[idSV] = {}
            dictSVtmp[f"{idSV}_concat"] = {}
            dictSV[idSV] = {}

        #Check if idSV had a caller in dict
        for caller in listCaller:
            if dictBrut[caller].get(idSV):

                dictSVtmp[idSV][caller] = dictBrut[caller][idSV]
                callerDetected.append(caller)
            else:
                dictSVtmp[idSV][caller] = []


        dictSVtmp[f"{idSV}_concat"]["QUAL"]   = []
        dictSVtmp[f"{idSV}_concat"]["FILTER"] = []
        dictSVtmp[f"{idSV}_concat"]["INFOS"]  = []
        dictSVtmp[f"{idSV}_concat"]["FORMAT"] = []


        #Iterate throught all caller possibly used
        #If callerSv is dict that's means variant is identify by caller
        #otherwise INFOS and FORMAT are None.
        #This allows you to order all INFOS
        #and FORMATS values in callers list
        #order
        for callerSV in listCaller:

            if isinstance(dictSVtmp[idSV][callerSV],dict):
                dictSV[idSV]["CHR"] = dictSVtmp[idSV][callerSV]["CHR"]
                dictSV[idSV]["POS"] = dictSVtmp[idSV][callerSV]["POS"]
                dictSV[idSV]["REF"] = dictSVtmp[idSV][callerSV]["REF"]
                dictSV[idSV]["ALT"] = dictSVtmp[idSV][callerSV]["ALT"]

                #Separate {idSV}_concat and idSV to put all INFOS and FORMATS
                #value in list for each category in order to concatenate this list
                # by | in str and put this str in idSV
                dictSVtmp[f"{idSV}_concat"]["QUAL"].append(dictSVtmp[idSV][callerSV]["QUAL"])   
                dictSVtmp[f"{idSV}_concat"]["FILTER"].extend(dictSVtmp[idSV][callerSV]["FILTER"]) 
                dictSVtmp[f"{idSV}_concat"]["INFOS"].append(dictSVtmp[idSV][callerSV]["INFOS"])  
                dictSVtmp[f"{idSV}_concat"]["FORMAT"].append(dictSVtmp[idSV][callerSV]["FORMAT"]) 
            else:                
                dictSVtmp[f"{idSV}_concat"]["INFOS"].append(None)
                dictSVtmp[f"{idSV}_concat"]["FORMAT"].append(None)



        for cat,value in dictSVtmp[f"{idSV}_concat"].items():

            if cat == "FILTER":
                
                if len(set(value)) == 0:
                    finalValue = ["."]
                else:
                    finalValue = value


            elif cat == "QUAL":

                listQUAL = list(filter(notNone,value))

                if len(listQUAL) == 0:
                    finalValue = "."
                else:
                    finalValue = statistics.median(listQUAL)


            elif cat == "FORMAT" or cat == "INFOS":
                tmpForInfoDict = {}
                tmpForInfoDictCorr = {}


                # Iterate through FORMAT and INFOS category
                # Put together all category
                # If not category detected (i.e if callerFormatInfo != None),
                # nothing is done.

                for indexCaller,callerFormatInfo in enumerate(value):

                    if callerFormatInfo != None:
                        # For each category in INFOS or FORMAT
                        for key in callerFormatInfo.keys():
                            if (key not in tmpForInfoDict.keys()) and (f"Nk{key}Nk" not in tmpForInfoDict.keys()):
                                #If category is in conflict between two caller
                                #add Nk on both side on category
                                if f"Nk{key}Nk" in NkIdsList:
                                    tmpForInfoDict[f"Nk{key}Nk"] = {}
                                else:
                                    tmpForInfoDict[key] = {}

                            if f"Nk{key}Nk" in NkIdsList:

                                if isinstance(callerFormatInfo[key],list):
                                    callerFormatInfo[key] = ','.join(map(str,callerFormatInfo[key]))

                                tmpForInfoDict[f"Nk{key}Nk"][indexCaller] = callerFormatInfo[key]
                            else:
                                if isinstance(callerFormatInfo[key],list):
                                    callerFormatInfo[key] = ','.join(map(str,callerFormatInfo[key]))

                                tmpForInfoDict[key][indexCaller] = callerFormatInfo[key]

                #Correct dict values with None values
                for vcfCat in tmpForInfoDict.keys():
                    for indexCaller, caller in enumerate(listCaller):
                        if bool(tmpForInfoDict[vcfCat].get(indexCaller)) == False:
                            #Add point to each category that does not used
                            #by caller
                            tmpForInfoDict[vcfCat][indexCaller] = "."


                # Transform dict values into string joined by pipe
                for vcfCat in tmpForInfoDict.keys():
                    listValCat = []

                    for subVal in range(0,len(tmpForInfoDict[vcfCat].keys())):

                        listValCat.append(tmpForInfoDict[vcfCat][subVal])

                    tmpForInfoDictCorr[vcfCat] = "|".join(map(str,listValCat))

                finalValue = copy.deepcopy(tmpForInfoDictCorr)

            #After category condition is has passed
            #finalValue is had to dictSV
                
            dictSV[idSV][cat] = finalValue

    if len(dictSV) != len(idSVList):

        raise Exception("""Differents size are detected between
                        dictSV and idSVList. Please check 
                        concatSV function""")
    
    return dictSV

def defineColumNames(id, listCaller):
    """
        Create list where each element
        is concatenate to id value.

    Args:
        id (str): Title of your category (ie SVTYPE;SVLEN)
        listCaller(list): Lister with caller name

    Returns:
        list: List with id add to caller name 
    """

    listFinale = []

    for caller in listCaller:
        name = id+"_"+caller
        listFinale.append(name)
    return "|".join(listFinale)

def reformatHeader(header,sample,listCaller):
    """
    Concatenate all VCFs header id 
    in same vcfpy.Header object, that will used
    to create the future vcf file with all
    SV.

    Args:
        header (list): List of all vcf header (vcfpy.Header)
        sample (str): Sample name
        listCaller (list): List with caller name

    Raises:
        Exception: If some difference are detected in contig size in VCF

    Returns:
        tuple: List of all lines of header, and each INFO or FORMAT field fill by "Nk"
    """

    new_header = vcfpy.Header(samples=vcfpy.SamplesInfos(sample_names=[sample]))
    new_header.add_line(vcfpy.HeaderLine("fileformat","VCFv4.3"))
    new_header.add_line(vcfpy.HeaderLine("date",date.today().strftime("%d/%m/%Y")))

    filterDict = {}
    contigDict = {}
    infoDict   = {}
    formatDict = {}
    NkIds      = []

    for headerVCF in header:
        for headLine in headerVCF.lines:

            if isinstance(headLine,vcfpy.header.FilterHeaderLine):
                if headLine.id not in filterDict.keys():
                    filterDict[headLine.id] = OrderedDict([("ID",headLine.id),
                                                            ("Description",
                                                            headLine.description)])
                else:
                    if headLine.description != filterDict[headLine.id]["Description"]:
                        logging.warning(
                            f"""
                                Different description detected for same id in your VCF FILTER {headLine.id}
                                    - {filterDict[headLine.id]["Description"]} (keeped by default)
                                    - {headLine.description}
                            """)
                        
            elif isinstance(headLine,vcfpy.header.ContigHeaderLine):
                if headLine.id not in contigDict.keys():
                    contigDict[headLine.id] = OrderedDict([("ID",headLine.id),
                                                                       ("length",headLine.length)])
                    
                else:
                    if headLine.length != contigDict[headLine.id]["length"]:
                        raise Exception(f"""
                                    Difference contig size {headLine.id}: 
                                    
                                    ({headLine.length} vs {contigDict[headLine.id]["length"]})
                                    
                                    """)
                    
            elif isinstance(headLine,vcfpy.header.FormatHeaderLine):

                if headLine.id not in formatDict.keys() and f"Nk{headLine.id}Nk" not in formatDict.keys():
                    formatDict[headLine.id] = OrderedDict([("ID",headLine.id),
                                                                       ("Number",1),
                                                                       ("Type","String"),
                                                                       ("Description",f"{headLine.description}. For caller {defineColumNames(headLine.id,listCaller)}")])
                    if headLine.id == "PL":
                        formatDict[headLine.id]["number"] = "G"


                elif  f"Nk{headLine.id}Nk" in formatDict.keys()    :
                    continue
                else:

                        formatDict.pop(headLine.id)

                        formatDict[f"Nk{headLine.id}Nk"] = OrderedDict([("ID",f"Nk{headLine.id}Nk"),
                                                                        ("Number",1),
                                                                        ("Type","String"),
                                                                        ("Description",f"{headLine.description}. For caller {defineColumNames(f'Nk{headLine.id}',listCaller)}")])
                        formatDict[headLine.id] = OrderedDict([("ID",headLine.id),
                                                                        ("Number",1),
                                                                        ("Type","String"),
                                                                        ("Description",headLine.description)])
                        NkIds.append(f"Nk{headLine.id}Nk")


            elif isinstance(headLine,vcfpy.header.InfoHeaderLine):



                if headLine.id not in infoDict.keys() and f"Nk{headLine.id}Nk" not in infoDict.keys():
                    infoDict[headLine.id] = OrderedDict([("ID",f"{headLine.id}"),
                                                                    ("Number",1),
                                                                    ("Type","String"),
                                                                    ("Description",f"{headLine.description}. For caller {defineColumNames(headLine.id,listCaller)}")
                                                                    ])
                    
                elif  f"Nk{headLine.id}Nk" in infoDict.keys():
                    continue
                else:
                    infoDict.pop(headLine.id)
                    infoDict[f"Nk{headLine.id}Nk"] = OrderedDict([("ID",f"Nk{headLine.id}Nk"),
                                                                       ("Number",1),
                                                                       ("Type","String"),
                                                                       ("Description",f"{headLine.description}. For caller {defineColumNames(f'Nk{headLine.id}',listCaller)}")])
                    infoDict[headLine.id] = OrderedDict([("ID",headLine.id),
                                                                       ("Number",1),
                                                                       ("Type","String"),
                                                                       ("Description",headLine.description)])
                    
                    NkIds.append(f"Nk{headLine.id}Nk")


    headerDict = {"FILTER":filterDict,"INFO":infoDict,"CONTIG":contigDict,"FORMAT":formatDict}              
    for cat in headerDict.keys():

        for id,line in headerDict[cat].items():

            if cat =="FILTER":
                new_header.add_filter_line(line)
            elif cat =="INFO":
                new_header.add_info_line(line)
            elif cat == "FORMAT":
                new_header.add_format_line(line)
            elif cat == "CONTIG":
                new_header.add_contig_line(line)
            elif cat == "ALT":
                new_header.add_line(line)

    new_header.add_line(vcfpy.HeaderLine("NkCNVCaller","|".join(listCaller)))

    
  

    return new_header,NkIds

def concatNkIds(formatDict,InfoDict,NkIdsList):
    """
    Concatenate each column with "Nk" words and same category

    Args:
        formatDict (dict): All format values for each variant
        InfoDict (dict): All INFO values for each variant
        NkIdsList (list): List of all column with Nk suffix and prefix

    Returns:
        Dict: Format and INFO column with new categories concatenated
    """
    x = lambda val : val != "."
    finalDict = {}

    for index,dictionnary in enumerate([formatDict,InfoDict]):
        tmpDict = {}
        for ids,value in dictionnary.items():
            if ids in NkIdsList:
                #Filter "."
                realValues = []
                valueSplitted = value.split("|")
                tmpRealValues = list(filter(x,valueSplitted))

                #Correct list if value is in list.
                for idVal in tmpRealValues:
                    if isinstance(idVal,list):
                        if len(idVal) == 1:
                            realValues.append(idVal[0])

                        else:
                            realValues.append(idVal)
                    else:
                        realValues.append(idVal)
     
                if len(realValues) == 0:
                    continue
                else:
                    count = Counter(realValues).most_common()

                    max= 1000
                    for pattern,count in count:

                        tmpDict[ids[2:-2]] = pattern

                        if count >= max:
                            tmpDict[ids[2:-2]] = "."
                        max = count

        dictionnary.update(tmpDict)
        if index == 0:
            finalDict["FORMAT"] = dictionnary
        else:
            finalDict["INFOS"] = dictionnary

    return finalDict

def writeVCF(folderAndName, dictVariant, new_header,sample,NkIdsList):
    writer = vcfpy.Writer.from_path(folderAndName, header=new_header)
    i=0
    for key,dictVar in dictVariant.items():
        # print(dictVar)
        # dictFormatUpdated = reformatFormatAndInfo(dictVar["FORMAT"])
        # dictInfosUpdated  = reformatFormatAndInfo(dictVar["INFOS"])

        # if len(dictFormatUpdated) != 0:

        try:
            dictVar["FORMAT"].pop("FT")
        except:
            pass

        dictFormatAndInfos = concatNkIds(dictVar["FORMAT"],
                                         dictVar["INFOS"],
                                         NkIdsList)
        dico_calls = [vcfpy.Call(sample,OrderedDict(dictFormatAndInfos["FORMAT"]))] 

        new_record = vcfpy.Record(dictVar["CHR"],dictVar["POS"],".",
                        dictVar["REF"],dictVar["ALT"],dictVar["QUAL"],
                        dictVar["FILTER"],dictFormatAndInfos["INFOS"],
                        list(dictFormatAndInfos["FORMAT"].keys()),dico_calls)

        writer.write_record(new_record)
        i+=1

    writer.close()
    return



def Main(vcf_list,listCallers,outputFolder):
    dictVariants    = {}
    dictAllVariants = {}
    listCodeSV      = []
    listHeader      = []
    listSample      = []
    callersUsedBrut = []


    
    listDictBrut    = Parallel(n_jobs=6)(delayed(collectSV)(vcfPath,
                                                        listCallerPossiblyUsed=listCallers) for vcfPath in vcf_list)
    
    #Concatenate all vcf dict in same dict
    for dictBrut in listDictBrut:
        
        for cat in dictBrut.keys():

            if cat == "sample":
                listSample.append(dictBrut["sample"])

            elif cat == "caller":
                callersUsedBrut.append(dictBrut["caller"])
            else:


                listCodeSV.extend(list(dictBrut[cat]["Variants"].keys()))

                dictVariants[cat] = dictBrut[cat]["Variants"]
                listHeader.append(dictBrut[cat]["Header"])


    if len(set(listSample)) > 1: 
        raise Exception(f"""
                        Multiple samples detected. 
                        Please check your vcf : {','.join(vcf_list)} 
                        """)
    else:
        sampleVcf = listSample[0]

    uniqueSV = list(set(listCodeSV))
    callersUsed = list(set(callersUsedBrut))

    new_header, NkIdsList = reformatHeader(header=listHeader,listCaller=callersUsed,sample=sampleVcf)

    print("\nSV concaténation",end="\n\n")
    listSVPreFormated = Parallel(n_jobs=16)(delayed(concatSV)(dictVariants,
                                                              codesVariant,
                                                              callersUsed,NkIdsList) 
                                                              for codesVariant in tqdm([uniqueSV[i:i+14]for i in range(0,
                                                                                                                       len(uniqueSV), 
                                                                                                                       14)]))

    for SVDict in listSVPreFormated:

            dictAllVariants.update(SVDict)

    print("Write VCF file",end="\n\n")

    writeVCF(outputFolder,dictAllVariants,new_header,
             sampleVcf,NkIdsList)


    return listCodeSV

parser = argparse.ArgumentParser(description="""Concatenate all SV vcf files from same sample.
                                                Please specify what caller you used in your vcf name
                                                and in caller list (arg -c)""",
                                 epilog=" Pierre GUERACHER - CHU Angers 2023")

parser.add_argument("-i", "--input", type=str,
                    help="""Input folder where VCF are located""",
                    required=True)
parser.add_argument("-o", "--output", type=str,
                    help="""Output folder""",
                    required=False, default="./")
parser.add_argument("-c", "--callers", type=list,nargs="+",
                    help="""List of caller used""",
                    required=False, default=["gatk","manta","pindel","cnvkit"])

args = parser.parse_args()

if __name__ == '__main__':
    listVCF = glob.glob(f"{args.input}/*[!SV].vcf")


    print(f"\nVCF used: {','.join(listVCF)}\n")
    Main(listVCF,args.callers,args.output)