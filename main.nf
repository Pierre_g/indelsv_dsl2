
include {   PINDEL         } from "./pindel.nf"
include {   MANTA          } from "./manta.nf"
include {   GATK           } from "./gatk4_cnv.nf"
include {   CNVKIT         } from "./cnvKit.nf"
include {   CONCATRESULTS  } from "./concatResults.nf"
include {   ANNOT          } from "./annotation.nf"

file(params.output).mkdir()
file(params.output+"/brut").mkdir()
file(params.output+"/brut/pindel" ).mkdir()
file(params.output+"/brut/manta").mkdir()
file(params.output+"/brut/gatk").mkdir()
file(params.output+"/brut/cnvkit").mkdir()
file(params.output+"/vcf").mkdir()
file(params.output+"/xlsx").mkdir()
file(params.output+"/annotSV").mkdir()


Channel
    .fromFilePairs("${params.input}/*{bam,bam.bai}")


    .set { bam_input }

workflow{

    bam_input
            .multiMap{ it ->
                topindel : it
                tomanta  : it
                togatk   : it
                tocnvkit : it
            }
            .set{multi_bam}

    PINDEL(multi_bam.topindel)
    MANTA(multi_bam.tomanta)
    GATK(multi_bam.togatk)
    CNVKIT(multi_bam.tocnvkit, params.bed_design)

    MANTA.out
            .join(GATK.out)
            .join(CNVKIT.out)
            .join(PINDEL.out)
            .map {it -> [it[0],it[1..-1]]}
            .view()
            .set{ vcfs_caller }

    CONCATRESULTS(vcfs_caller)
    ANNOT(CONCATRESULTS.out)
    

}