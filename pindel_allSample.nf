

process FILTER_BAM{ //Get only paired reads

    cpus 15
    memory '10G'
    tag "${ID_ECH}"
    input:
    tuple val(ID_ECH),path(BAM_FILE)

    output:

    path("${ID_ECH}_SAMTAG1.{bam,bam.bai}"), emit: toPindel
    path("${ID_ECH}_SAMTAG1.{bam,bam.bai}"), emit: toConfig
    script:
    """
    samtools view -f 1 ${BAM_FILE[0]} -h -b -@${task.cpus} > ${ID_ECH}_SAMTAG1.bam
    samtools index ${ID_ECH}_SAMTAG1.bam
    """
}


process PINDEL_CONFIG{
    // publishDir path:"${params.pindel_output}",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '30G'
    input:
    path(BAM_FILE)

    output:

    path("config_${params.project_name}.txt")


    script:
    """
    for file in `ls \$(realpath .)/*bam`;do
        PI_INFO=\$(samtools view -H  \$file | grep '^@RG' | grep -wo "PI:[0-9]*" | grep -wo "[0-9]*")
        
        name=\$(basename \$file _SAMTAG1.bam)
        echo "\$file \$PI_INFO \$name" >> config_${params.project_name}.txt

    done
    """
}

process PINDEL{
    publishDir path:"${params.pindel_output}",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    input:
    path(BAM_COLLECT)
    path(config_pindel)


    output:
    path("${params.project_name}_pindel.vcf")

    script:
    def date = new Date().format("yyyy-MM-dd")
    """
    ${params.pindel} --fasta \
    ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa \
    -i ${config_pindel} -o ${params.project_name}_pindel --min_distance_to_the_end 8 \
    --number_of_threads ${task.cpus} --window_size 5 --sequencing_error_rate 0.01 \
    --sensitivity 0.95 --maximum_allowed_mismatch_rate 0.02 --NM 2 --report_long_insertions \
    --report_breakpoints --report_interchromosomal_events --min_perfect_match_around_BP 3 \
    --min_inversion_size 50 --min_num_matched_bases 30 --anchor_quality 0 --minimum_support_for_event 1 \
    --genotyping --detect_DD --MAX_DISTANCE_CLUSTER_READS 100 --MIN_DD_CLUSTER_SIZE 3 --MIN_DD_BREAKPOINT_SUPPORT 3\
    --MIN_DD_MAP_DISTANCE 8000 --DD_REPORT_DUPLICATION_READS


    
    ${params.pindel2vcf} -r ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa \
    -R ${params.genome} -d ${date} -P ${params.project_name}_pindel_brut  \
    --min_coverage 10 --het_cutoff 0.25 --hom_cutoff 0.8 --min_supporting_reads 1 --max_internal_repeats infinite \
    --max_internal_repeatlength infinite --max_postindel_repeats  infinite --max_postindel_repeatlength infinite


    bcftools reheader --fai ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa.fai \
    -o ${params.project_name}_pindel.vcf --threads 6 ${params.project_name}_pindel_brut.vcf

    """ 

}

process SPLITVCF {

    publishDir path:"${params.pindel_output}",mode: 'copy', overwrite: true, failOnError:true
    cpus 4
    memory '10G'
    input:
    tuple val(sample), path(pindelVCF)

    output:

    tuple val(sample),path("${sample}_pindel.vcf")

    script:

    """
    bcftools view -c1 -Ov -s ${sample} -o ${sample}_pindel.vcf ${pindelVCF}
    """    
}


workflow pindel_workflow{

    take:
        data

    main:

        data.multiMap{channel ->
                sampleList:channel[0]
                toFILTERBAM:channel}
            .set{ data_pindel }
        

        FILTER_BAM(data_pindel.toFILTERBAM)

        PINDEL_CONFIG(FILTER_BAM.out.toConfig.collect())


        PINDEL(FILTER_BAM.out.toPindel.collect(),PINDEL_CONFIG.out)

        data_pindel.sampleList.combine(PINDEL.out).view().set{ PINDELVCF }


        SPLITVCF(PINDELVCF)

}