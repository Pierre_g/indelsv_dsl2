

process FILTER_BAM{ //Get only paired reads

    cpus 15
    memory '10G'
    tag "${ID_ECH}"
    tag "${sample}"
    input:
    tuple val(ID_ECH),path(BAM_FILE)

    output:

    tuple val(ID_ECH), path("${ID_ECH}_SAMTAG1.{bam,bam.bai}")

    script:
    """
    samtools view -f 1 ${BAM_FILE[0]} -h -b -@${task.cpus} > ${ID_ECH}_SAMTAG1.bam
    samtools index ${ID_ECH}_SAMTAG1.bam
    """
}



process PINDELPROC{
    publishDir path:"${params.output}/brut/pindel",mode: 'copy', overwrite: true, failOnError:true
    cpus 6
    memory '200G'
    tag "${sample}"
    input:
        tuple val(sample),path(bam)
    output:
         tuple val(sample),path("${sample}_pindel_brut*")

    script:

    """
    PI_INFO=\$(samtools view -H  ${bam[0]} | grep '^@RG' | grep -wo "PI:[0-9]*" | grep -wo "[0-9]*")
    name=\$(basename ${bam[0]} _SAMTAG1.bam)
    echo "${sample}_SAMTAG1.bam \$PI_INFO \$name" >> config_${sample}.txt

    #Get X% of read to arg  min_num_matched_bases
    minMatchNumBase=\$(samtools view ${bam[0]} | awk '{print length(\$10)}'  | head -n 10000 | awk '{readLenght+=\$1;next} END{print int((readLenght/NR)*0.30)}')


    ${params.pindel} --fasta \
    ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa \
    -i config_${sample}.txt -o ${sample}_pindel_brut --min_distance_to_the_end 8 \
    --number_of_threads ${task.cpus} --window_size 5 --sequencing_error_rate 0.01 \
    --sensitivity 0.99 --maximum_allowed_mismatch_rate 0.02 --NM 2 --report_long_insertions \
    --report_breakpoints --report_interchromosomal_events --min_perfect_match_around_BP 3 \
    --min_inversion_size 50 --min_num_matched_bases \$minMatchNumBase --anchor_quality 20 --minimum_support_for_event 10 \
    --genotyping --detect_DD --MAX_DISTANCE_CLUSTER_READS 100 --MIN_DD_CLUSTER_SIZE 3 --MIN_DD_BREAKPOINT_SUPPORT 3\
    --MIN_DD_MAP_DISTANCE 8000 --DD_REPORT_DUPLICATION_READS

    """ 
// Sensitivity 0.99
//min_num_matched_bases 75
// anchor_quality 20
// min support event
}

process CREATEVCF {

    publishDir path:"${params.output}/brut/pindel",mode: 'copy', overwrite: true, failOnError:true
    cpus 4
    memory '10G'
    tag "${sample}"
    input:
    tuple val(sample), path(pindel_output)

    output:

    tuple val(sample),path("${sample}_pindel.vcf")

    script:
    def date = new Date().format("yyyy-MM-dd")
    """
    ${params.pindel2vcf} -r ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa \
    -R ${params.genome} -d ${date} -P ${sample}_pindel_brut  \
    --min_coverage 10 --het_cutoff 0.25 --hom_cutoff 0.8 --min_supporting_reads 1 --max_internal_repeats infinite \
    --max_internal_repeatlength infinite --max_postindel_repeats  infinite --max_postindel_repeatlength infinite


    bcftools reheader --fai ${params.genome_dir}/${params.genome}/genome/${params.genome}.fa.fai \
    -o ${sample}_pindel.vcf --threads 6 ${sample}_pindel_brut.vcf

    """    
}


workflow PINDEL {

    take:
        data

    main:
       

        FILTER_BAM(data)

        PINDELPROC(FILTER_BAM.out)

        CREATEVCF(PINDELPROC.out)

    emit:
        CREATEVCF.out
        // val("pindel")

}